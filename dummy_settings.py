from _project.settings import *

# STANDARD SETTINGS
SITE_URL = ''
SITE_ROOT = 'https://' + SITE_URL
ALLOWED_HOSTS = [SITE_URL, ]
SECRET_KEY = ''
DEBUG = False