from django.http import HttpResponse, JsonResponse
import weasyprint
import chardet
def generate_pdf(request):
	url = ""
	message = ""
	pdf_bytestring = ""
	if request.POST:
		url = request.POST.get('url', None)
	if url :
		try :
			pdf_bytestring = weasyprint.HTML(url).write_pdf()
			print(chardet.detect(pdf_bytestring)['encoding'])
			return HttpResponse(pdf_bytestring, content_type="application/pdf")
		except Exception as e:
			message = e
	else :
		message = "Please provide a URL !"
	return ask_for_url(request, url, message, pdf_bytestring)
	
def ask_for_url(request, url="", message="", pdf_bytestring=""):
	return HttpResponse('''
		<p style="color: darkred;">{}</p>
		<form method="POST">
			URL:<br/>
			<input name="url" value="{}"><br/>
			<button>Convert</button>
		</form>
		<br/>
		{}
		'''.format(message, url, pdf_bytestring))
