import os

######################################################################

PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
ROOT_URLCONF = '_project.urls'
WSGI_APPLICATION = '_project.wsgi.application'
APPEND_SLASH = True

MIDDLEWARE = [
	'django.middleware.security.SecurityMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

INSTALLED_APPS = ['pdf',]


#  Local settings : import local_settings if exist
try: from local_settings import *
except ImportError: pass