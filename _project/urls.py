from django.urls import path

from pdf import views

# URL Patterns
urlpatterns = [
	path('', views.generate_pdf),
]